import React from 'react';
import RootNavigator from '../navigation/Root';
// import UserProvider from '../../providers/UserProvider';

export function App() {
  return <RootNavigator />;
}

export default App;
