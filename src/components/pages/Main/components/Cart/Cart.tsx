import React, { useContext, useState, useEffect, useCallback, MouseEvent } from 'react';
import { CartContext, getProducts } from 'services';
import { Product, ProductId } from 'interfaces';
import { CartActionType, CartAction } from '../../Main';
import { Paper, List } from '@material-ui/core';
import { CartItem } from '..';
import { find } from 'lodash';

export function Cart() {
  const { cart, dispatchCart } = useContext(CartContext);
  const [products, setProducts] = useState<Product[]>([]);
  // eslint-disable-next-line
  const [totalCollectionCount, setTotalCollectionCount] = useState(0);

  useEffect(() => {
    setTimeout(() => {
      getProducts(0, 10).then(({ collection, totalCollectionCount }) => {
        setProducts(collection);
        setTotalCollectionCount(totalCollectionCount);
      });
    }, 500);
  }, []);

  const fireAction = useCallback(
    (productId: ProductId, type: CartActionType) => (e: MouseEvent<HTMLButtonElement>) => {
      e.preventDefault();
      dispatchCart({ type, productId } as CartAction);
    },
    [dispatchCart],
  );

  return (
    <div>
      <h2>Your cart</h2>
      {cart.length === 0 ? <h3>Your cart is empty.</h3> : null}
      {cart.length !== 0 ? (
        <Paper>
          <List>
            {cart.map(({ id, count }) => (
              <CartItem id={id} key={id} count={count} product={find(products, { id })} fireAction={fireAction} />
            ))}
          </List>
        </Paper>
      ) : null}
    </div>
  );
}
