import React, { MouseEvent } from 'react';
import { ListItem, ListItemText, IconButton, ListItemSecondaryAction } from '@material-ui/core';
import { Delete as DeleteIcon, Add as AddIcon, Remove as RemoveIcon } from '@material-ui/icons';
import { ProductId, Product } from 'interfaces';
import { CartActionType } from '../../Main';

interface CartItemProps {
  id: ProductId;
  count: number;
  product: Product | undefined;
  fireAction: (productId: ProductId, type: CartActionType) => (e: MouseEvent<HTMLButtonElement>) => void;
}

export function CartItem({ id, product, count, fireAction }: CartItemProps) {
  return (
    <ListItem>
      <ListItemText primary={product?.name} secondary={`Count: ${count}`} />
      <ListItemSecondaryAction>
        <IconButton onClick={fireAction(id, CartActionType.INCREMENT)}>
          <AddIcon />
        </IconButton>
        <IconButton onClick={fireAction(id, CartActionType.DECREMENT)}>
          <RemoveIcon />
        </IconButton>
        <IconButton onClick={fireAction(id, CartActionType.REMOVE_FROM_CART)}>
          <DeleteIcon />
        </IconButton>
      </ListItemSecondaryAction>
    </ListItem>
  );
}

// export function CartItem({ id, product, count, fireAction }: CartItemProps) {
//   return (
//       <>
//         <p>{id}</p>
//         <p>{count}</p>
//         <p>{product?.name}</p>
//         <p>{product?.price}</p>
//       </>
//   );
// }
