import React, { useCallback, useEffect, useState, useContext } from 'react';
import { Redirect } from 'react-router-dom';
import { cond, find, matches } from 'lodash';

import { ProductCard, CartItem } from './components';
import { AuthContext, getProducts, CartContext } from '../../../services';
import { Product, ProductId } from '../../../interfaces';

export type CartItem = { id: ProductId; count: number; product?: Product | undefined };
export type DispatchCart = (action: CartAction) => void;

export enum CartActionType {
  ADD_TO_CART = 'ADD_TO_CART',
  REMOVE_FROM_CART = 'REMOVE_FROM_CART',
  INCREMENT = 'INCREMENT',
  DECREMENT = 'DECREMENT',
  CHANGE_AMOUNT = 'CHANGE_AMOUNT',
}

export default function Main() {
  const [products, setProducts] = useState<Product[] | null>(null);
  // eslint-disable-next-line
  const [totalCollectionCount, setTotalCollectionCount] = useState(0);

  const { cart, dispatchCart } = useContext(CartContext);
  const { user } = useContext(AuthContext);

  const addToCart = useCallback(
    (productId: ProductId) => {
      dispatchCart({ type: CartActionType.ADD_TO_CART, productId } as CartAction);
    },
    [dispatchCart],
  );

  useEffect(() => {
    setTimeout(() => {
      getProducts(0, 10).then(({ collection, totalCollectionCount }) => {
        setProducts(collection);
        setTotalCollectionCount(totalCollectionCount);
      });
    }, 500);
  }, []);

  return (
    <>
      {user === null && <Redirect to="/signin" />}
      <div>
        <h2>PRODUCT PAGE!</h2>
        {products === null && <p>Loading....</p>}
        {products !== null && products.length === 0 && <p>List is empty </p>}
        {products !== null &&
          products.length > 0 &&
          products.map((product) => (
            <ProductCard {...product} isInCart={isInCart(product.id, cart)} addToCart={addToCart} key={product.id} />
          ))}
      </div>
    </>
  );
}

export type CartAction = { type: CartActionType; productId: ProductId } & {
  type: CartActionType;
  productId: ProductId;
  count: number;
};

export function cartReducer(state: CartItem[] = [], action: CartAction): CartItem[] {
  return cond([
    [
      matches({ type: CartActionType.ADD_TO_CART }),
      () => {
        const inArray = Boolean(find(state, { id: action.productId }));
        return inArray ? state : [...state, { id: action.productId, count: 1 }];
      },
    ],
    [matches({ type: CartActionType.REMOVE_FROM_CART }), () => state.filter(({ id }) => id !== action.productId)],
    [
      matches({ type: CartActionType.INCREMENT }),
      () => state.map(({ id, count }) => ({ id, count: id === action.productId ? count + 1 : count })),
    ],
    [
      matches({ type: CartActionType.DECREMENT }),
      () => state.map(({ id, count }) => ({ id, count: Math.max(id === action.productId ? count - 1 : count, 1) })),
    ],
    [
      matches({ type: CartActionType.CHANGE_AMOUNT }),
      () => state.map((product) => (product.id === action.productId ? { ...product, count: action.count } : product)),
    ],
    [() => true, () => state],
  ])(action);
}

function isInCart(id: ProductId, cart: CartItem[]) {
  return Boolean(find(cart, { id }));
}
