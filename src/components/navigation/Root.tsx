import React, { useContext } from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';

import SignIn from '../pages/SignIn/SignIn';
import SignUp from '../pages/SignUp/SignUp';
import Main from '../pages/Main/Main';
import { AuthContextProvider, CartContextProvider, AuthContext } from '../../services';
import { Cart } from 'components/pages/Main/components/Cart/Cart';
import { Container, makeStyles } from '@material-ui/core';
import { Copyright } from 'components/components/Copyright/Copyright';
import AppNavBar from 'components/components/AppNavBar';

const useStyles = makeStyles((theme) => ({
  main: {
    flexGrow: 1,
    flexShrink: 0,
  },
}));

function RootNavigator() {
  const classes = useStyles();
  const { user } = useContext(AuthContext);

  console.log(user);
  return (
    <BrowserRouter>
      <AuthContextProvider>
        <CartContextProvider>
          {user === null ? <AppNavBar /> : null}
          <Container maxWidth="lg" component="main" className={classes.main}>
            <Route exact path="/">
              <Redirect to="/signin" />
            </Route>
            <Switch>
              <Route path="/main">
                <Main />
              </Route>
              <Route path="/product/:id">
                <h1>Single product</h1>
              </Route>
              <Route path="/cart">
                <Cart />
              </Route>
            </Switch>
            <Switch>
              <Route path="/signin">
                <SignIn />
              </Route>
              <Route path="/signup">
                <SignUp />
              </Route>
            </Switch>
          </Container>
          <Copyright />
        </CartContextProvider>
      </AuthContextProvider>
    </BrowserRouter>
  );
}

export default RootNavigator;
