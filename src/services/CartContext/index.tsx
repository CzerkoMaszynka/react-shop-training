import React, { createContext, useReducer, PropsWithChildren } from 'react';
import { CartItem, cartReducer } from 'components/pages/Main/Main';

interface CartContextProps {
  readonly state?: CartItem[];
  readonly dispatch?: React.Dispatch<any>;
}

interface CartContextType {
  cart: CartItem[];
  dispatchCart: React.Dispatch<any>;
}

export const CartContext = createContext<CartContextType>({
  cart: [],
  dispatchCart: () => null,
});

export function CartContextProvider({ children }: PropsWithChildren<CartContextProps>) {
  const [state, dispatch] = useReducer(cartReducer, []);

  return <CartContext.Provider value={{ cart: state, dispatchCart: dispatch }}>{children}</CartContext.Provider>;
}
