import firebase from 'firebase';

const firebaseConfig = {
  apiKey: 'AIzaSyAvRnv83p-iY1lQCWBkXkSNZ5VmMjaQMEk',
  authDomain: 'react-spyro-shop.firebaseapp.com',
  databaseURL: 'https://react-spyro-shop.firebaseio.com',
  projectId: 'react-spyro-shop',
  storageBucket: 'react-spyro-shop.appspot.com',
  messagingSenderId: '247936381924',
  appId: '1:247936381924:web:be88357ea1d6ff4cfecdf1',
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const firebaseAuth = firebaseApp.auth();

export const firebaseService = {
  async signInWithGoogle() {
    await firebaseAuth.useDeviceLanguage();
    await firebaseAuth.setPersistence(firebase.auth.Auth.Persistence.LOCAL);
    return await firebaseAuth.signInWithRedirect(new firebase.auth.GoogleAuthProvider());
  },

  signInWithEmailAndPassword(email: string, password: string) {
    firebaseAuth.signInWithEmailAndPassword(email, password).catch((error) => console.log(error));
  },

  createUserWithEmailAndPassword(email: string, password: string) {
    firebaseAuth.createUserWithEmailAndPassword(email, password).catch((error) => console.log(error));
  },

  getRedirectResult() {
    return firebaseAuth.getRedirectResult();
  },

  signOut() {
    return firebaseAuth.signOut();
  },

  onAuthStateChanged: firebaseAuth.onAuthStateChanged.bind(firebaseAuth),
};
